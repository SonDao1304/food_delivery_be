const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const app = express();

dotenv.config();
mongoose.connect(process.env.MONGODB_URL);
app.use(bodyParser.json({ limit: '50mb' }));
app.use(morgan('common'));
const port = process.env.PORT || 8000;
app.use(cors());
app.use('/api/v1', require('./src/api/routes'));
app.listen(port);
