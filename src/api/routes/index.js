const express = require('express');
const routes = express.Router();

routes.use('/auth', require('./auth.route'));
routes.use('/restaurant', require('./restaurant.route'));
routes.use('/food', require('./food.route'));
routes.use('/tag', require('./tag.route'));
module.exports = routes;
