const routes = require('express').Router();
const tagController = require('../controllers/tag.controller');

routes.post('/addtag', tagController.addTags);

module.exports = routes;
