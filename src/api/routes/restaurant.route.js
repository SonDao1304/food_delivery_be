const routes = require('express').Router();
const restaurantController = require('../controllers/restaurant.controller');

routes.post('/register', restaurantController.register);
routes.get('/getAll', restaurantController.getAll);
routes.post('/addtag', restaurantController.addTags);

module.exports = routes;
