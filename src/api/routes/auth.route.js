const routes = require('express').Router();
const authController = require('../controllers/auth.controller');
const jwtMiddleWare = require('../middleware/jwt.middleware');

routes.post('/register', authController.register);
routes.post('/login', authController.login);

module.exports = routes;
