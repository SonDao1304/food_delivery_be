const routes = require('express').Router();
const foodController = require('../controllers/food.controller');

routes.post('/add', foodController.add);

module.exports = routes;
