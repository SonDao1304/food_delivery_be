const mongoose = require('mongoose');
const foodSchema = new mongoose.Schema({
  name: { type: String, default: null },
  description: { type: String, default: null },
  price: { type: Number, default: null },
  image: { type: String, default: null },
});
const Food = mongoose.model('Food', foodSchema);
module.exports = Food;
