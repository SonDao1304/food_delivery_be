const mongoose = require('mongoose');
const restaurantSchema = new mongoose.Schema({
  name: { type: String, default: null },
  mobile_number: { type: String, default: null },
  email: { type: String, default: null },
  address: { type: String, default: null },
  rating: { type: Number, default: null },
  tags: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Tag' }],
  image: { type: String, default: null },
});
const Restaurant = mongoose.model('Restaurant', restaurantSchema);
module.exports = Restaurant;
