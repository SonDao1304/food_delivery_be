const mongoose = require('mongoose');
const tagSchema = new mongoose.Schema({
  title: { type: String, default: null },
});
const Tag = mongoose.model('Tag', tagSchema);
module.exports = Tag;
