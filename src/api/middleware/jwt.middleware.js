const jwt = require('jsonwebtoken');

const jwtMiddleWare = (req, res, next) => {
  const token = req.header.token;
  if (token) {
    const accessToken = token.split(' ')[1];
    jwt.verify(accessToken, process.env.SECRET_KEY, (err, user) => {
      if (err) {
        return res.status(403).json('Token is not valid');
      }
      req.user = user;
      return next();
    });
  } else {
    return res.status(403).send('A token is required for authentication');
  }
};
module.exports = jwtMiddleWare;
