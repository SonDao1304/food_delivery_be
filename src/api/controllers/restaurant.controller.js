const Restaurant = require('../models/restaurant.model');
const Tag = require('../models/tags.model');
const restaurantController = {
  register: async (req, res, next) => {
    try {
      console.log('call');
      const { name, email, mobile_number, address, image, tags, rating } =
        req.body;
      console.log({ name, email, mobile_number, address, image, tags, rating });
      const restaurants = await Restaurant.create({
        name,
        email,
        mobile_number,
        address,
        image,
        rating,
        tags,
      });
      const tagsResponse = tags.map();
      res.status(201).json(restaurant);
    } catch (err) {
      console.log(err);
    }
  },
  getAll: async (req, res, next) => {
    try {
      const restaurants = await Restaurant.find().populate('tags');
      res.status(200).json(restaurants);
      console.log(restaurants);
    } catch (error) {
      console.log(error);

      res.status(500).json(error);
    }
  },
  addTags: async (req, res, next) => {
    try {
      const { title } = req.body;
      const tag = await Tag.create({ title });
      res.status(200).json(tag);
    } catch (error) {
      res.status(500).json(error);
    }
  },
};
module.exports = restaurantController;
