const Tag = require('../models/tags.model');
const tagController = {
  addTags: async (req, res, next) => {
    try {
      const { title } = req.body;
      const tag = await Tag.create({ title });
      res.status(200).json(tag);
    } catch (error) {
      res.status(500).json(error);
    }
  },
};
module.exports = tagController;
