const Food = require('../models/food.model');
const foodController = {
  add: async (req, res, next) => {
    try {
      const { name, description, price, image } = req.body;
      const food = await Food.create({
        name,
        description,
        price,
        image,
      });
      res.status(201).json(food);
    } catch (err) {
      console.log(err);
    }
  },
};
module.exports = foodController;
