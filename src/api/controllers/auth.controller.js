const User = require('../models/user.model');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const authController = {
  register: async (req, res, next) => {
    try {
      const { first_name, last_name, email, password, mobile_number, gender } =
        req.body;
      if (!(email && password && first_name && last_name)) {
        res.status(400).send('All input is required');
      }
      const oldUser = await User.findOne({ email });
      if (oldUser) {
        return res.status(409).send('User Already Exist. Please Login');
      }
      encryptedPassword = await bcrypt.hash(password, 10);
      const user = await User.create({
        first_name,
        last_name,
        email: email.toLowerCase(),
        password: encryptedPassword,
        mobile_number,
        gender,
      });
      res.status(201).json(user);
    } catch (err) {
      console.log(err);
    }
  },
  login: async (req, res, next) => {
    try {
      const { email, password } = req.body;
      if (!(email && password)) {
        res.status(400).send('All input is required');
      }
      const user = await User.findOne({ email });
      if (user && (await bcrypt.compare(password, user.password))) {
        const token = jwt.sign(
          { user_id: user._id, email },
          process.env.SECRET_KEY,
          {
            expiresIn: '2h',
          }
        );
        res.status(200).json({ user, token });
      }
      res.status(400).send('Invalid Credentials');
    } catch (err) {
      console.log(err);
    }
  },
};
module.exports = authController;
